pres
========================================================
author: 
date: 

First Slide
========================================================

## Introduction

* The main idea is to teach a machine learning algorithm to predict esophageal cancer using esoph data, so that regular people will be able to calculate their probability to have such cancer.

* The algorithm is a general linear regression model with 10-fold cross-validation.

* The data used is esoph dataset from datasets package.



```r
data(esoph)
head(esoph,4)
```

```
  agegp     alcgp    tobgp ncases ncontrols
1 25-34 0-39g/day 0-9g/day      0        40
2 25-34 0-39g/day    10-19      0        10
3 25-34 0-39g/day    20-29      0         6
4 25-34 0-39g/day      30+      0         5
```

Slide With Code
========================================================
## Summary of the data


```r
summary(esoph)
```

```
   agegp          alcgp         tobgp        ncases         ncontrols    
 25-34:15   0-39g/day:23   0-9g/day:24   Min.   : 0.000   Min.   : 1.00  
 35-44:15   40-79    :23   10-19   :24   1st Qu.: 0.000   1st Qu.: 3.00  
 45-54:16   80-119   :21   20-29   :20   Median : 1.000   Median : 6.00  
 55-64:16   120+     :21   30+     :20   Mean   : 2.273   Mean   :11.08  
 65-74:15                                3rd Qu.: 4.000   3rd Qu.:14.00  
 75+  :11                                Max.   :17.000   Max.   :60.00  
```



Slide With Plot
========================================================

## The model


```r
Usedata<-esoph
index<-createDataPartition(y=Usedata$p, p=0.8, list=F)
set_training<-Usedata[index,]
set_testing<-Usedata[-index,]

Lmodel<-train(p ~ agegp + tobgp * alcgp, data=set_training, method="glm", trControl = trainControl(method="cv"))

summary(Lmodel)
```

```

Call:
NULL

Deviance Residuals: 
     Min        1Q    Median        3Q       Max  
-0.44741  -0.10720  -0.02868   0.12439   0.60516  

Coefficients:
                    Estimate Std. Error t value Pr(>|t|)    
(Intercept)        0.3642050  0.0309959  11.750 3.99e-16 ***
agegp.L            0.4504370  0.0768323   5.863 3.36e-07 ***
agegp.Q           -0.0747488  0.0773776  -0.966   0.3386    
agegp.C           -0.0318273  0.0748872  -0.425   0.6726    
`agegp^4`          0.0893371  0.0720986   1.239   0.2210    
`agegp^5`         -0.0212795  0.0708401  -0.300   0.7651    
tobgp.L            0.1137936  0.0591952   1.922   0.0602 .  
tobgp.Q            0.0737665  0.0622108   1.186   0.2412    
tobgp.C            0.0298578  0.0642960   0.464   0.6444    
alcgp.L            0.3965229  0.0604218   6.563 2.67e-08 ***
alcgp.Q            0.0605622  0.0615583   0.984   0.3298    
alcgp.C            0.0421759  0.0638278   0.661   0.5117    
`tobgp.L:alcgp.L` -0.0700967  0.1146669  -0.611   0.5437    
`tobgp.Q:alcgp.L`  0.0109111  0.1215090   0.090   0.9288    
`tobgp.C:alcgp.L`  0.0005469  0.1315368   0.004   0.9967    
`tobgp.L:alcgp.Q` -0.0774939  0.1173157  -0.661   0.5119    
`tobgp.Q:alcgp.Q` -0.1228325  0.1229439  -0.999   0.3225    
`tobgp.C:alcgp.Q` -0.0113596  0.1273883  -0.089   0.9293    
`tobgp.L:alcgp.C`  0.0128207  0.1223063   0.105   0.9169    
`tobgp.Q:alcgp.C`  0.0125680  0.1252304   0.100   0.9205    
`tobgp.C:alcgp.C`  0.0432785  0.1270294   0.341   0.7347    
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

(Dispersion parameter for gaussian family taken to be 0.06059613)

    Null deviance: 8.9424  on 71  degrees of freedom
Residual deviance: 3.0904  on 51  degrees of freedom
AIC: 21.645

Number of Fisher Scoring iterations: 2
```

========================================================
## Algorithm performance on a test set

* The first figure shows the real probability 

![plot of chunk unnamed-chunk-6](pres-figure/unnamed-chunk-6-1.png)

* The next one is the predicted probability. The performance is obviously not good.

![plot of chunk unnamed-chunk-7](pres-figure/unnamed-chunk-7-1.png)

