---
title: "documentation"
author: "Otryakhin Dmitry"
date: "21 February 2016 �."
output: html_document
---

# Documentation for Esophageal cancer explorer.

## Introduction
This software was created as a course project for Developing Data Products online course, which is a part of [Data Science specialization](https://www.coursera.org/specializations/jhu-data-science). The main idea of this program is not innovative, you can find a lot of similar (and working much better) programs on the internet.

## GUI
The graphical user interface consist of 4 tab panels: "About the data", "Predict", "Help" and "Disclamer".

### About the data
On this panel, we can see a graph of a probability to have cancer based on age and alcohol and tabaco consumption. A colour represents age group while size represents tobaco consumption. Alcohol consumption is depicted on the horizontal axis. The graph was created using ggplot2 package in R.

### Predictions
To predict a probability for a patient to have cancer, choose the appropriate values of alcohol and tobaco consumption and, of course age on the right panel. After that you will see the corresponding probability on the main panel, calculated by the algorithm. Sometimes a probability could be less than zero or more than 100%. It is because the esoph dataset is rather small for a machine learning algorithm. It has just 88 observations for 5 factorial variables. In that sence it's not a good idea to use the software for real medical tasks. Another reason is that I'm using a linear regression model.